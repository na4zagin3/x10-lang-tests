#!/bin/bash

#X10CXX=~/workspace-work/x10/x10.dist/bin/x10c++
X10CXX=x10c++

source ./tap.sh

mkdir results

NTESTS=$(ls team/*/*.x10 | wc -l )
begin_test results/build.tup $NTESTS
mkdir bin
cd bin
for name in ../team/*/*.x10
do
	$X10CXX -o ${name%.x10} $name ; is_ok "build: $name"
done
end_test

begin_test results/test.tup $NTESTS
mkdir bin
cd bin
for name in ../team/*/*.x10
do
	mpirun -np 8 -host sc01,sc02,sc03,sc04,sc05,sc06,sc07,sc08 ${name%.x10} ; is_ok "run: $name"
done
end_test
