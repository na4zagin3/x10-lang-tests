#!/bin/bash

declare -i TEST_DEPTH
declare -i TEST_COUNTER
TEST_FILE=/dev/null

begin_test () {
	TEST_FILE=$1
	TEST_COUNTER=0
	echo "1..$2" >$TEST_FILE
}

end_test () {
	return
}

diag () {
	echo "# $1" >>$TEST_FILE
}

is_ok () {
	RESULT=$?
	TEST_COUNTER=$(( TEST_COUNTER + 1 ))
	if [[ $RESULT -eq 0 ]] ; then
		echo "ok $TEST_COUNTER $1" >>$TEST_FILE
	else
		echo "not ok $TEST_COUNTER $1" >>$TEST_FILE
	fi
}

